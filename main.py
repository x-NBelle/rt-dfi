#!/usr/bin/env python3.8
# coding=utf-8

import os
from datetime import datetime
from pathlib import Path

import pulp

from benchmarks import benchmarks
from optDfi.experiment.FileSystem import FileSystem, DryRunFS
from optDfi.experiment.experiment import experiment_iteration_vs_witness
from optDfi.misc.utils import print_color, Color

import config

def check_industrial_config() -> bool :
    """
    Checks if the industrial tools are installed on the system
    """
    cplex_present = "CPLEX_CMD" in pulp.listSolvers( True )
    return config.DEFAULT_AIT_PATH is not None and cplex_present

def main() :
    time_limit = 3 * 60
    slack = 0  # Final formula is 1 + (slack/100)
    iteration = 4
    timestamp = datetime.now().isoformat()
    exp_name = f"{time_limit}s-{slack}"
    fs = FileSystem( Path( os.getcwd() ) / f"experiment/{timestamp}/{exp_name}/" )
    result_file = Path( fs.base_dir / "result.csv" )
    timing_file = Path( fs.base_dir / "timing.csv" )

    with open( result_file, 'a' ) as f :
        f.write( f"BENCH,WITNESS_WCET,SOTA_WCET,VALUE_ANALYSIS_WCET," + ",".join(
            [ f"ILP_{i}_WCET" for i in range( iteration ) ] ) + ",IT_NUM\n" )
    with open( timing_file, 'a' ) as f :
        f.write(
            f"BENCH,WITNESS_BUILD,WITNESS_WCET,SOTA_BUILD,SOTA_WCET,PRE_VALUE_BUILD,PRE_VALUE_WCET,VALUE_ANALYSIS,VALUE_ANALYSIS_BUILD,VALUE_ANALYSIS_WCET," + ",".join(
                [ f"ILP_{i}_ANALYSIS,ILP_{i}_BUILD,ILP_{i}_WCET" for i in range( iteration ) ] ) + "\n" )

    for bench in benchmarks :
        # try :
            if check_industrial_config():
                bench_fs = DryRunFS( fs.base_dir, fs.base_dir, bench.tag )
            else:
                bench_fs = DryRunFS( base_input=config.DRY_RUN_FOLDER_PATH, base_output=fs.base_dir, bench_name=bench.tag )

            experiment_iteration_vs_witness( bench, bench_fs, result_file, timing_file, time_limit, slack, iteration )

        # except Exception as e :
        #     print_color( "BENCHMARK EXCEPTION RAISED : " + str( e ), Color.RED )
        #     print_color( "BENCHMARK INTERRUPTED : ", Color.RED )


if __name__ == '__main__' :
    main()
