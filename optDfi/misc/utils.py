# coding=utf-8
from enum import IntEnum
from pathlib import Path
import config


class Color( IntEnum ):
    BLACK = 30
    RED = 31
    GREEN = 32
    ORANGE = 33
    BLUE = 34
    MAGENTA = 35
    CYAN = 36
    LIGHT_GRAY = 37
    DEFAULT = 39

def print_color( s: str, color: Color, *args, **kwargs ):
    print( f"\033[1;{color}m{s}\033[0m", *args, **kwargs )

def llvm_exe_path( exe: str, llvm_path: Path = config.DEFAULT_LLVM_PATH ) -> Path :
    return llvm_path / "build" / "bin" / exe

def llvm_lib_path( libname: str, llvm_path: Path = config.DEFAULT_LLVM_PATH ) -> Path :
    return llvm_path / "build" / "lib" / libname