# coding=utf-8
import re
import time
from shutil import copyfile

from pathlib import Path
from typing import List, Optional, Dict
import subprocess as sp

import config
from optDfi.optimizations.ilp_opt.tag_check_ilp_opt import optimize_wcep_ilp, EmptyProblemException, \
    UnsolvedILPException
from optDfi.optimizations.improve_reachability_analysis import improve_reachability_analysis
from optDfi.compilation.DFILib import execute_clang, execute_opt, execute_phasar, execute_llc, execute_gcc
from optDfi.experiment.FileSystem import FileSystem, DryRunFS
from optDfi.misc.utils import Color, print_color
from optDfi.exceptions import FileNotFoundException
from optDfi.experiment.Benchmark import Benchmark


# Building part
def build_common( bench: Benchmark, fs: FileSystem ) :
    execute_clang( sources=bench.sources,
                   output_file=fs.linked_ll,
                   includes=bench.includes,
                   flags=bench.clang_flags )

    execute_opt( entry_file=fs.linked_ll,
                 output_file=fs.aligned_ll,
                 passes=[ "O1", "dfialignment" ],
                 flags=bench.opt_flags )


def build_IR_to_exes( ll_file: Path, asm_file: Path,
                      exe_qemu_file: Path,
                      llc_flags: List[ str ], gcc_flags: List[ str ] ) :
    execute_llc( entry_file=ll_file,
                 output_file=asm_file,
                 flags=llc_flags )

    # WCET
    execute_gcc( entry_file=asm_file,
                 output=exe_qemu_file,
                 linker_script=config.LINKER_SCRIPTS[ "qemu-wcet" ],
                 flags=gcc_flags )


def build_witness( bench: Benchmark, fs: FileSystem ) :
    build_IR_to_exes(
        ll_file=fs.aligned_ll,
        asm_file=fs.witness_asm,
        exe_qemu_file=fs.witness_qemu_exe_path,
        llc_flags=bench.llc_flags,
        gcc_flags=bench.gcc_flags
    )


def build_dfi_prebackend( bench: Benchmark, fs: FileSystem ) :
    execute_opt( entry_file=fs.aligned_ll,
                 output_file=fs.annotated_ll,
                 passes=[ "dfiannotation" ],
                 flags=bench.opt_flags )

    execute_phasar( entry_file=fs.annotated_ll,
                    output_file=fs.analysed_ll )

    execute_opt( entry_file=fs.analysed_ll,
                 output_file=fs.iteration_optimized_ll,
                 passes=bench.opt_passes + [ "dfiintrinsicinstr" ],
                 flags=bench.opt_flags )


def build_dfi_protected( bench: Benchmark, fs: FileSystem ) :
    build_IR_to_exes(
        ll_file=fs.iteration_optimized_ll,
        asm_file=fs.iteration_asm,
        exe_qemu_file=fs.it_bin_qemu,
        llc_flags=bench.llc_flags,
        gcc_flags=bench.gcc_flags
    )


def gen_ais_file( entry_file: Path, output_file: Path, executable: Path, dfi: bool ) :
    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )
    if not executable.exists() :
        raise FileNotFoundException( executable )

    with open( output_file, "w" ) as output :
        output.write( "try {\n" )

        with open( entry_file, "r" ) as entry :
            output.writelines( entry.readlines() )

    if dfi :
        ebreak_inf_cmd1 = "{objdump} -d {exe}".format(
            objdump=config.DEFAULT_RISCV32_OBJDUMP_PATH,
            exe=executable
        )
        ebreak_inf_cmd2 = "| grep \"ebreak\" | awk '{ sub(\":\", \"\", $1); print \"instruction 0x\" $1 \"-0x4 condition always taken;\" }'"
        ebreak_inf_cmd3 = f">> {output_file}"

        ebreak_inf_cmd = " ".join( [ ebreak_inf_cmd1, ebreak_inf_cmd2, ebreak_inf_cmd3 ] )

        p = sp.run( ebreak_inf_cmd, shell=True, stdout=sp.PIPE, text=True, encoding='utf-8' )

        if p.returncode != 0 :
            raise Exception( "Failed to execute the objdump command" )

    with open( output_file, "a" ) as f :
        f.write( "\nsuppress message: 3103;\n" )
        f.write( "}" )


def gen_apx_files( executable: Path, ais_file: Path, output_file: Path, xml_file: Path, txt_report: Path,
                   entry_point: str = "main" ) :
    template: Optional[ str ] = None

    with open( config.APX_TEMPLATE_PATH, "r" ) as f :
        template = "".join( f.readlines() )

    apx = template.format(
        executable=executable,
        xml_report=xml_file,
        ais=ais_file,
        txt_report=txt_report,
        analysis_start=entry_point
    )

    with open( output_file, "w" ) as f :
        f.write( apx )


def prepare_aiT_witness_files( bench: Benchmark, fs: FileSystem ) :
    gen_ais_file( bench.ais_file, fs.witness_ais, fs.witness_qemu_exe_path, dfi=False )
    gen_apx_files( fs.witness_qemu_exe_path, fs.witness_ais, fs.witness_apx, fs.witness_xml, fs.witness_txt,
                   bench.entry_point )


def prepare_aiT_dfi_files( bench: Benchmark, fs: FileSystem, entry_point: str ) :
    gen_ais_file( entry_file=bench.ais_file,
                  output_file=fs.it_aiT_ais,
                  executable=fs.it_bin_qemu,
                  dfi=True )
    gen_apx_files( executable=fs.it_bin_qemu,
                   ais_file=fs.it_aiT_ais,
                   output_file=fs.it_aiT_apx,
                   xml_file=fs.it_aiT_xml,
                   txt_report=fs.it_aiT_txt,
                   entry_point=entry_point )


def execute_aiT( entry_file: Path, apx_file: Path ) :
    """Execute aiT"""
    if config.DEFAULT_AIT_PATH is None :
        return

    command_line = [ str( config.DEFAULT_AIT_PATH ), "-b", str( apx_file ) ]
    cmd = " ".join( command_line )
    p = sp.run( cmd, shell=True )

    if p.returncode != 0 :
        raise Exception( "aiT return status != 0 for " + str( entry_file ) )


def retrieve_wcet( xml_file: Path ) -> int :
    wcet_re = re.compile( "<wcet>(\d*) cycles</wcet>" )

    with open( xml_file, "r" ) as f :
        for line in f :
            m = wcet_re.match( line )
            if m is not None :
                return int( m.group( 1 ) )

    return -1


class TimeManager :
    def __init__( self, time_dict: Dict[ str, int ], name: str ) :
        self.time_dict = time_dict
        self.name = name
        self.start_time = None

    def __enter__( self ) :
        self.start_time = time.perf_counter()

    def __exit__( self, exc_type, exc_val, exc_tb ) :
        end_time = time.perf_counter()
        self.time_dict[ self.name ] = end_time - self.start_time


def experiment_iteration_vs_witness( bench: Benchmark, fs: DryRunFS, result_file: Path, timing_file: Path,
                                     time_limit: int, slack: int, number_iteration: int ) :
    timings = { }

    build_common( bench, fs.output_fs )

    with TimeManager( timings, "witness_build" ) :
        build_witness( bench, fs.output_fs )

    with TimeManager( timings, "witness_wcet" ) :
        prepare_aiT_witness_files( bench, fs.output_fs )
        execute_aiT( entry_file=fs.input_fs.witness_qemu_exe_path,
                     apx_file=fs.output_fs.witness_apx )
        witness_wcet = retrieve_wcet( fs.input_fs.witness_xml )

    fs_sota = DryRunFS( base_input=fs.input_fs.base_dir,
                        base_output=fs.output_fs.base_dir,
                        bench_name=fs.input_fs.bench_name,
                        it_num=0 )

    with TimeManager( timings, "sota_build" ) :
        build_dfi_prebackend( bench, fs_sota.output_fs )
        build_dfi_protected( bench, fs_sota.output_fs )

    with TimeManager( timings, "sota_wcet" ) :
        prepare_aiT_dfi_files( bench, fs_sota.output_fs, entry_point=bench.entry_point )
        execute_aiT( entry_file=fs_sota.input_fs.it_bin_qemu,
                     apx_file=fs_sota.output_fs.it_aiT_apx )

        it0_wcet = retrieve_wcet( xml_file=fs_sota.input_fs.it_aiT_xml )

    fs_pre_value_analysis = DryRunFS( base_input=fs_sota.input_fs.base_dir,
                                      base_output=fs_sota.output_fs.base_dir,
                                      bench_name=fs_sota.input_fs.bench_name,
                                      it_num=1 )
    copyfile( fs_sota.output_fs.iteration_optimized_ll, fs_pre_value_analysis.output_fs.iteration_optimized_ll )
    fs_value_analysis = DryRunFS( base_input=fs_pre_value_analysis.input_fs.base_dir,
                                  base_output=fs_pre_value_analysis.output_fs.base_dir,
                                  bench_name=fs_pre_value_analysis.input_fs.bench_name,
                                  it_num=2 )

    with TimeManager( timings, "pre_value_build" ) :
        build_dfi_protected( bench, fs_pre_value_analysis.output_fs )

    with TimeManager( timings, "pre_value_wcet" ) :
        prepare_aiT_dfi_files( bench, fs_pre_value_analysis.output_fs, entry_point="main" )
        execute_aiT( entry_file=fs_pre_value_analysis.input_fs.it_bin_qemu,
                     apx_file=fs_pre_value_analysis.output_fs.it_aiT_apx )

    with TimeManager( timings, "value_analysis" ) :
        improve_reachability_analysis( xml_file=fs_pre_value_analysis.input_fs.it_aiT_xml,
                                       binary_file=fs_pre_value_analysis.input_fs.it_bin_qemu,
                                       original_ll_file=fs_pre_value_analysis.input_fs.iteration_optimized_ll,
                                       optimized_ll_file=fs_value_analysis.output_fs.iteration_optimized_ll,
                                       log_file=fs_value_analysis.output_fs.it_problem_log )

    with TimeManager( timings, "value_analysis_build" ) :
        build_dfi_protected( bench, fs_value_analysis.output_fs )

    with TimeManager( timings, "value_analysis_wcet" ) :
        prepare_aiT_dfi_files( bench, fs_value_analysis.output_fs, entry_point=bench.entry_point )
        execute_aiT( entry_file=fs_value_analysis.input_fs.it_bin_qemu,
                     apx_file=fs_value_analysis.output_fs.it_aiT_apx )
        it_value_analysis_wcet = retrieve_wcet( xml_file=fs_value_analysis.input_fs.it_aiT_xml )

    solution = None
    previousItFs = fs_value_analysis

    wcets = [ it_value_analysis_wcet ]
    it_num = 0
    try :
        for iteration in range( fs_value_analysis.input_fs.it_num + 1,
                                fs_value_analysis.input_fs.it_num + 1 + number_iteration ) :
            nextItFs = DryRunFS( base_input=fs.input_fs.base_dir,
                                 base_output=fs.output_fs.base_dir,
                                 bench_name=fs.input_fs.bench_name,
                                 it_num=iteration )

            with TimeManager( timings, f"ilp_{it_num}_analysis" ) :
                solution = optimize_wcep_ilp( xml_file=previousItFs.input_fs.it_aiT_xml,
                                              binary_file=previousItFs.input_fs.it_bin_qemu,
                                              original_ll_file=previousItFs.input_fs.iteration_optimized_ll,
                                              optimized_ll_file=nextItFs.output_fs.iteration_optimized_ll,
                                              previous_solution=solution,
                                              problem_log=nextItFs.output_fs.it_problem_log,
                                              ilp_pb_stats=nextItFs.output_fs.it_ilp_stat_log,
                                              time_limit=time_limit,
                                              slack=slack,
                                              lp_file=nextItFs.output_fs.it_ilp_pb_file,
                                              solution_json=nextItFs.input_fs.it_ilp_solution_json,
                                              solution_file=nextItFs.input_fs.it_ilp_solution_file )

            with TimeManager( timings, f"ilp_{it_num}_build" ) :
                build_dfi_protected( bench, nextItFs.output_fs )

            with TimeManager( timings, f"ilp_{it_num}_wcet" ) :
                prepare_aiT_dfi_files( bench, nextItFs.output_fs, entry_point=bench.entry_point )
                execute_aiT( entry_file=nextItFs.input_fs.it_bin_qemu,
                             apx_file=nextItFs.output_fs.it_aiT_apx )

                wcets.append( retrieve_wcet( xml_file=nextItFs.input_fs.it_aiT_xml ) )

            previousItFs = nextItFs
            it_num += 1

    except EmptyProblemException :
        print( "Stopped due to empty problem" )
    except UnsolvedILPException :
        print_color( "Stopped due to unresolved ILP Exception", Color.RED )

    first_wcet = wcets[ 1 ] if len( wcets ) >= 2 else wcets[ 0 ]
    best_wcet = min( wcets )

    print_color(
        f"{bench.tag:15}:{witness_wcet:10}|{it0_wcet:10}|{it_value_analysis_wcet:10}|{first_wcet:10}|{best_wcet:10}",
        color=Color.GREEN )

    result_line = ",".join( [ f"{bench.tag},{witness_wcet},{it0_wcet},{it_value_analysis_wcet}" ] + [
        f"{wcets[ i + 1 ] if len( wcets ) >= 2 else wcets[ 0 ]}" for i in range( number_iteration )
    ] + [ f"{it_num}\n" ] )
    with open( result_file, 'a' ) as f :
        f.write( result_line )

    timing_line = ",".join( [ str( bench.tag ), str( timings[ 'witness_build' ] ), str( timings[ 'witness_wcet' ] ),
                              str( timings[ 'sota_build' ] ), str( timings[ 'sota_wcet' ] ),
                              str( timings[ "pre_value_build" ] ), str( timings[ "pre_value_wcet" ] ),
                              str( timings[ "value_analysis" ] ), str( timings[ "value_analysis_build" ] ),
                              str( timings[ "value_analysis_wcet" ] ),
                              ] + [
                                f"{timings.get( f'ilp_{i}_analysis', 0 )},{timings.get( f'ilp_{i}_build', 0 )},{timings.get( f'ilp_{i}_wcet', 0 )}"
                                for i in range( number_iteration )
                            ] ) + "\n"
    with open( timing_file, 'a' ) as f :
        f.write( timing_line )
