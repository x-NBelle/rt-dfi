# coding=utf-8
from typing import Optional, Dict, List, Any
import networkx as nx

from optDfi.interface.a3.misc import BBData
from optDfi.misc.graph_representation import print_graph


class CFG( nx.DiGraph ) :
    """
    Class to represents a CFG of a function
    """
    bbdata_id: str = "bbdata"

    def __init__( self, attrib: Dict[ str, str ], *args, **kwargs ) :
        super().__init__( *args, **kwargs )
        self.range: Optional[ BBData ] = None
        self.id_name_map: Dict[ str, str ] = { }
        self.attrib = attrib
        self.real_bb: List[ str ] = [ ]

    def add_bb( self, bbid: str, attrib: Dict[ str, str ], bbname: str, bbdata: Optional[ BBData ] = None ) :
        self.add_node( bbid, **{ **attrib, self.bbdata_id : bbdata } )
        self.id_name_map[ bbid ] = bbname

        if bbdata is not None :
            self.real_bb.append( bbid )

        if bbdata is not None :
            if self.range is None :
                self.range = bbdata
            else :
                self.range = BBData( first_instruction=min( self.range.first_instruction, bbdata.first_instruction ),
                                     last_instruction=max( self.range.last_instruction, bbdata.last_instruction ) )

    def find_bb_with_add( self, address: int ) -> Optional[ str ] :
        for bb in self.real_bb :
            r = self.nodes[ bb ][ self.bbdata_id ]
            if r.first_instruction <= address <= r.last_instruction :
                return bb

        # raise Exception("Failed to find Basic Block with the given address")
        return None

    def print( self, filename="cfg" ) :
        graph = nx.DiGraph()

        graph.add_edges_from(
            map( lambda e : (self.id_name_map[ e[ 0 ] ], self.id_name_map[ e[ 1 ] ]),
                 self.edges ) )

        print_graph( graph, filename )


class ICFG( nx.DiGraph ) :
    """
    Class to represents an Inter-procedural CFG
    """
    _cfg_id: str = "cfg"
    _block_calling_id: str = "callIds"

    def __init__( self, *args, **kwargs ) :
        super().__init__( *args, **kwargs )
        self.id_name_map: Dict[ str, str ] = { }

    def add_function( self, fid: str, fname: str, attrib: Dict[ str, Any ] ) :
        self.add_node( fid, **{ self._cfg_id : CFG( name=fname, attrib=attrib ) } )
        self.id_name_map[ fid ] = fname

    def getCFG( self, fid: Optional[ str ] = None, fname: Optional[ str ] = None ) -> CFG :
        if fid is None and fname is None :
            raise Exception( "getCFG without providing fid or fname" )

        if fid is not None :
            return self.nodes[ fid ][ self._cfg_id ]

        for cfgid, name in self.id_name_map.items() :
            if name == fname :
                return self.nodes[ cfgid ][ self._cfg_id ]

        raise Exception( "Tried to getCFG with an invalid name" )

    def add_call( self, caller_id: str, callee_id: str, bb_id: str ) :
        # Case we have a real call
        self.add_edge( caller_id, callee_id )

        if self[ caller_id ][ callee_id ].get( self._block_calling_id, None ) is None :
            self[ caller_id ][ callee_id ][ self._block_calling_id ] = [ bb_id ]
        else :
            self[ caller_id ][ callee_id ][ self._block_calling_id ].append( bb_id )

    def print( self, show_unresolved=False, filename="icfg" ) :
        graph = nx.DiGraph()

        graph.add_edges_from(
            filter( lambda e : "Unresolved" not in e[ 1 ],
                    map( lambda e : (self.id_name_map[ e[ 0 ] ], self.id_name_map[ e[ 1 ] ]),
                         self.edges ) ) )

        print_graph( graph, filename )
