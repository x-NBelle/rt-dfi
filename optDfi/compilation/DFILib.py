# coding=utf-8

import os
import subprocess as sp
import config
from pathlib import Path
from typing import List, Optional

from optDfi.exceptions import FileNotFoundException


def llvm_exe_path( exe: str, llvm_path: Path = config.DEFAULT_LLVM_PATH ) -> Path :
    return llvm_path / "build" / "bin" / exe


def llvm_lib_path( libname: str, llvm_path: Path = config.DEFAULT_LLVM_PATH ) -> Path :
    return llvm_path / "build" / "lib" / libname


def execute_clang( sources: List[ Path ], output_file: Path, includes: Optional[ List[ Path ] ] = None, flags: Optional[ List[ str ] ] = None ) :
    """ Execute Clang compiler to generate a LLVM IR result_file """
    if includes is None :
        includes = [ ]
    if flags is None :
        flags = [ ]

    # Be sure to be in the right directory
    curcwd = os.getcwd()
    tmp_dir = output_file.absolute().parent
    os.chdir( str( tmp_dir ) )

    # Append a few flags and files at the command line
    command_line = [ llvm_exe_path( "clang" ) ]
    command_line += [ f"-I{include}" for include in includes ]

    command_line += flags + [ "-S", "-emit-llvm" ]
    command_line += list(map(str, sources))

    if len( sources ) != 1 :
        # If there is more than 1 llvm-module, we have to compile all the
        # module then we have to combine them with llvm-link

        # First generate the llvm-modules
        sp.run( command_line )

        # Now create a new command line to combine them
        linker_command_line = [ llvm_exe_path( "llvm-link" ) ]
        for source in sources :
            stem = source.stem
            linker_command_line.append( str( tmp_dir / (stem + ".ll") ) )

        linker_command_line.append( "-S" )
        linker_command_line.append( "-o" )
        linker_command_line.append( str( output_file ) )

        sp.run( linker_command_line )

    else :
        # We just the single llvm-module
        command_line.append( "-o" )
        command_line.append( str( output_file ) )

        sp.run( command_line )

    os.chdir( curcwd )


def execute_opt( entry_file: Path, output_file: Path, passes: List[ str ], flags: Optional[ List[ str ] ] = None ) :
    """ Execute OPT to execute optimization passes on LLVM IR """

    if flags is None :
        flags = [ ]

    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )

    command_line = [ llvm_exe_path( "opt" ) ]

    command_line += config.ARCH_FLAGS
    # command_line += config.OPT_FLAGS

    command_line += [ "-load", llvm_lib_path( "LLVMDFI.so" ) ]
    command_line += [ f"--{pass_name}" for pass_name in passes ]
    command_line += flags
    command_line += [ "-S", "-o", str( output_file ), str( entry_file ) ]

    sp.run( command_line )


def execute_phasar( entry_file: Path, output_file: Path ) :
    """ Execute Phasar to perform the DFI analysis """

    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )

    command_line = [ config.DEFAULT_PHASAR_PATH, str( entry_file ), str( output_file ) ]

    sp.run( command_line )


def execute_llc( entry_file: Path, output_file: Path, flags: Optional[ List[ str ] ] = None ) :
    """ Execute LLC to perform backend transformation of LLVM IR to assembly code """

    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )

    if flags is None :
        flags = [ ]

    command_line = [ llvm_exe_path( "llc" ) ]

    command_line += config.ARCH_FLAGS
    command_line += flags
    command_line += [ str( entry_file ), "-o", str( output_file ) ]

    sp.run( command_line )


def execute_gcc( entry_file: Path, output: Path, linker_script: str,
                 debug: bool = True, flags: Optional[ List[ str ] ] = None ) :
    """ Execute GCC to compile and link assembly code """
    if not entry_file.exists() :
        raise FileNotFoundException( entry_file )

    if flags is None :
        flags = [ ]

    command_line = [ str( config.DEFAULT_GCC_RISCV32_PATH ) ]
    command_line += flags
    command_line += [ str( entry_file ), f"{config.DEFAULT_LINK_PATH}/crt.s", f"{config.DEFAULT_LINK_PATH}/lib.c" ]

    if debug :
        command_line.append( "-g" )

    command_line.append( "-T" )
    command_line.append( f"{config.DEFAULT_LINK_PATH}/{linker_script}" )
    command_line.append( "-o" )
    command_line.append( str( output ) )

    a = sp.run( command_line )