# coding=utf-8
import pickle

from pathlib import Path
from pprint import pprint

from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import *
from optDfi.optimizations.ilp_opt.ilp_problem.context import *
from optDfi.optimizations.ilp_opt.ilp_problem.problem import *

IIP = IlpIntervalProblem

def load_problem( problem_file: Path ) -> IIP :
    with open( problem_file, 'rb' ) as file :
        return pickle.load( file )

def restrict_context_path_by_lid( cp: ContextPath, loadids: Set[ LoadId ] ) -> ContextPath:
    return ContextPath(
        { lid: context for lid, context in cp.load_contexts.items() if lid in loadids }
    )

def view_loadid( problem: IlpIntervalProblem, loadids: Set[ LoadId ] ) -> IIP :
    widen_loads = { lid for loadid in loadids for lid in problem.eqclass_loads[ loadid ] }

    restricted_eqclass_loads = EqClassMap( widen_loads )
    for cur_class in problem.eqclass_loads.current_cls:
        if cur_class in widen_loads:
            for lid in problem.eqclass_loads[ cur_class ]:
                restricted_eqclass_loads.fusion_element_class( cur_class, lid )

    new_visualization = IlpIntervalProblem(
        path_contexts=restrict_context_path_by_lid( problem.path_contexts, widen_loads ),
        constraints=[ (restrict_context_path_by_lid( cs, widen_loads ), cost) for cs, cost in problem.constraints ],
        eqclass_loads=restricted_eqclass_loads,
        eqclass_tags=problem.eqclass_loads,
    )

    return new_visualization
