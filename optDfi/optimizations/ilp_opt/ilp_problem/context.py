# coding=utf-8
from __future__ import annotations

from dataclasses import dataclass
from typing import Tuple, List, Set, Iterable, FrozenSet, Dict

import pipe

from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import EqClassMap

Tag = str
TagRepr = int
Interval = Tuple[ int, int ]
LoadId = str


@dataclass( frozen=True )
class ExactContext( object ) :
    tag: Tag
    count: int


@dataclass( frozen=True )
class PartialContext( object ) :
    tags: FrozenSet[ Tag ]
    count: int


@dataclass( frozen=True )
class UnknownContext( object ) :
    count: int


@dataclass
class LoadContexts( object ) :
    loadid: LoadId
    exacts: List[ ExactContext ]
    partial: List[ PartialContext ]
    unknown: List[ UnknownContext ]
    tags: Set[ Tag ]

    def __init__( self, loadid: LoadId,
                  exacts: Iterable[ ExactContext ] = (),
                  partial: Iterable[ PartialContext ] = (),
                  unknown: Iterable[ UnknownContext ] = (),
                  tags: Iterable[ Tag ] = () ) :
        self.exacts = list( exacts )
        self.partial = list( partial )
        self.unknown = list( unknown )
        self.tags = set( tags )
        self.loadid = loadid

    @staticmethod
    def _fusion_count( contexts: List ) -> int :
        return sum( contexts | pipe.map( lambda c : c.count ) )

    def _fusion_unknown( self ) -> List[ UnknownContext ] :
        if len( self.unknown ) == 0 :
            return [ ]

        return [ UnknownContext( count=self._fusion_count( self.unknown ) ) ]

    def _fusion_exacts( self ) -> List[ ExactContext ] :
        if len( self.exacts ) == 0 :
            return [ ]

        return list(
            self.exacts
            | pipe.groupby( keyfunc=lambda context : context.tag )
            | pipe.map(
                lambda tag_context : ExactContext(
                    tag=tag_context[ 0 ],
                    count=self._fusion_count( tag_context[ 1 ] )
                )
            )
        )

    def _fusion_partial( self ) -> List[ PartialContext ] :
        if len( self.partial ) == 0 :
            return [ ]

        partial_context_dict: Dict[ FrozenSet[ Tag ], List[ PartialContext ] ] = dict()

        for partial in self.partial :
            if partial.tags not in partial_context_dict :
                partial_context_dict[ partial.tags ] = [ ]

            partial_context_dict[ partial.tags ].append( partial )

        return list(
            map( lambda tag_contexts : PartialContext(
                tags=tag_contexts[ 0 ],
                count=self._fusion_count( tag_contexts[ 1 ] )
            ), partial_context_dict.items() )
        )

    def fusion_optimization( self ) -> LoadContexts :
        return LoadContexts(
            exacts=self._fusion_exacts(),
            partial=self._fusion_partial(),
            unknown=self._fusion_unknown(),
            tags=self.tags,
            loadid=self.loadid
        )

    def remove_null_count_contexts( self ) -> LoadContexts :
        return LoadContexts(
            unknown=list( self.unknown | pipe.where( lambda x : x.count > 0 ) ),
            partial=list( self.partial | pipe.where( lambda x : x.count > 0 ) ),
            exacts=list( self.exacts | pipe.where( lambda x : x.count > 0 ) ),
            tags=self.tags,
            loadid=self.loadid
        )

    def apply_eqclass_tags( self, eqclass_tags: EqClassMap[ Tag ] ) -> LoadContexts :
        exacts: List[ ExactContext ] = (self.exacts | pipe.map(
            lambda context : ExactContext(
                tag=eqclass_tags.witness( context.tag ),
                count=context.count
            )
        ))

        partial: List[ PartialContext ] = (
                self.partial | pipe.map( lambda context : PartialContext(
            tags=frozenset( eqclass_tags.witness( t ) for t in context.tags ),
            count=context.count
        )
                                         )
        )

        return LoadContexts(
            loadid=self.loadid,
            exacts=exacts,
            partial=partial,
            unknown=self.unknown,
            tags={ eqclass_tags.witness( t ) for t in self.tags }
        )

    def apply_tag_map( self, tag_map: Dict[ Tag, Tag ] ) -> LoadContexts :
        for tag in self.tags:
            if tag not in tag_map:
                pass

        return LoadContexts(
            loadid=self.loadid,
            exacts=[ ExactContext( tag=tag_map[ ec.tag ], count=ec.count ) for ec in self.exacts ],
            partial=[ PartialContext( tags=frozenset( { tag_map[ tag ] for tag in pc.tags } ), count=pc.count ) for pc
                      in self.partial ],
            unknown=self.unknown,
            tags={ tag_map[ tag ] for tag in self.tags }
        )
