# coding=utf-8
from __future__ import annotations

from dataclasses import dataclass
from itertools import chain, combinations
from typing import TypeVar, Generic, Set, Dict, Iterator, Iterable, Callable, Any, Sized, Hashable, Optional, FrozenSet

T = TypeVar( 'T', bound=Hashable )
Predicat = Callable[ [ T ], bool ]


@dataclass
class EqData( Generic[ T ], Iterable[ T ], Sized ) :
    equivalent_set: Set[ T ]
    witness: Optional[ T ]

    def __init__( self, iterable: Iterable[ T ] = (), witness: Optional[T] = None ) :
        self.equivalent_set = set( iterable )
        if witness is not None and witness not in self.equivalent_set:
            raise Exception( f"Witness {witness} is not present in the equivalent set {self.equivalent_set}" )
        self.witness = witness

    def __iter__( self ) -> Iterator[ T ] :
        return self.equivalent_set.__iter__()

    def __eq__( self, other: object ) -> bool :
        if not isinstance( other, EqData ) :
            return NotImplemented
        return self.witness == other.witness and self.equivalent_set == other.equivalent_set

    def __contains__( self, item: T ) -> bool :
        return item in self.equivalent_set

    def __len__( self ) -> int :
        return len( self.equivalent_set )

    def update( self, other: EqData ) -> None:
        self.equivalent_set.update( other.equivalent_set )


@dataclass
class EqClass( Generic[ T ], Iterable[ T ], Sized ) :
    data: EqData[ T ]

    def __init__( self, iterable: Iterable[ T ] = (), witness: Optional[T] = None ) :
        self.data = EqData( iterable=iterable, witness=witness )

    def __iter__( self ) -> Iterator[ T ] :
        return self.data.__iter__()

    def __eq__( self, other: object ) -> bool :
        if not isinstance( other, EqClass ) :
            return NotImplemented
        return self.data == other.data

    def __contains__( self, item: T ) -> bool :
        return item in self.data

    def __len__( self ) -> int :
        return len( self.data )

    def union( self, other: EqClass[ T ] ) :
        self.data.update( other.data )
        other.data = self.data

    @property
    def witness( self ) -> Optional[ T ] :
        return self.data.witness


@dataclass
class EqClassMap( Generic[ T ], Iterable[ T ] ) :
    map: Dict[ T, EqClass[ T ] ]
    current_cls: Set[ T ]

    def __init__( self, iterable: Iterable[ T ] = () ) :
        self.map = { t : EqClass( iterable=(t,), witness=t ) for t in iterable }
        self.current_cls = set( self.map )

    def __getitem__( self, item: T ) -> FrozenSet[ T ] :
        return frozenset( self.map[ item ].data )

    def witness( self, item: T ) -> Optional[T]:
        return self.map[ item ].data.witness

    def __contains__( self, item: T ) -> bool :
        return item in self.map

    def __setitem__( self, key: T, value: Any ) -> None :
        raise Exception( f"Equivalent classes should not be manipulated directly: {key}" )

    def __iter__( self ) -> Iterator[ T ] :
        return self.map.__iter__()

    def classes( self ) -> Iterator[ T ] :
        return self.current_cls.__iter__()

    def __and__( self, other: EqClassMap[ T ] ) -> EqClassMap[ T ] :
        new_clsmap = EqClassMap( chain( self.__iter__(), other.__iter__() ) )

        for t1 in self.classes() :
            for t2 in self.map[ t1 ] :
                if t1 not in other and t2 not in other :
                    new_clsmap.fusion_element_class( t1, t2 )
                elif t1 in other and t2 in other and other.check_equivalence( t1, t2 ) :
                    new_clsmap.fusion_element_class( t1, t2 )

        for t1 in other.classes() :
            for t2 in other.map[ t1 ] :
                if t1 not in self and t2 not in self :
                    new_clsmap.fusion_element_class( t1, t2 )
                elif t1 in self and t2 in self and self.check_equivalence( t1, t2 ) :
                    new_clsmap.fusion_element_class( t1, t2 )

        return new_clsmap

    def check_equivalence( self, e1: T, e2: T ) -> bool :
        return self.map[ e1 ] == self.map[ e2 ]

    def fusion_element_class( self, e1: T, e2: T ) -> None :
        if e1 != e2 and self.map[ e1 ].witness != self.map[ e2 ].witness:
            discarded = self.map[ e2 ].witness
            self.map[ e1 ].union( self.map[ e2 ] )
            if discarded is not None :
                self.current_cls.discard( discarded )

    def add_elements( self, iterable: Iterable[ T ], allow_duplicate: bool = False ) -> None :
        for t in iterable :
            if t in self :
                if not allow_duplicate :
                    raise Exception( f"Duplicate found in the EqClassMap : {t}" )
            else :
                self.map[ t ] = EqClass( (t,) )
                self.current_cls.add( t )

    @staticmethod
    def construct_eqclass_map( elements: Iterable[ T ], equiv: Callable[ [ T, T ], bool ] ) -> EqClassMap[ T ] :
        new_eqclass = EqClassMap( elements )

        for e1, e2 in combinations( new_eqclass.map, r=2 ) :
            if equiv( e1, e2 ) :
                new_eqclass.fusion_element_class( e1, e2 )

        return new_eqclass
