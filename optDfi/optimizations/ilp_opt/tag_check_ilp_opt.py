# coding=utf-8
from __future__ import annotations

import pickle
from pathlib import Path
from typing import List, Tuple, Dict, Set, Optional

import pipe
import pulp

import config
from optDfi.interface.a3.context_extraction import extract_wcep_context_data_from_xml, A3_VALUE_ANALYSIS, LOAD_ADD_HEX, \
    A3ValueAnalysisContext
from optDfi.interface.objdump import extract_dfi_loads, DFILoad
from optDfi.optimizations.ilp_opt.ilp_problem.context import ExactContext, PartialContext, UnknownContext, \
    LoadContexts, Tag, LoadId
from optDfi.optimizations.ilp_opt.ilp_problem.equivalence_map import EqClassMap
from optDfi.optimizations.ilp_opt.ilp_problem.problem import IlpIntervalProblem, ContextPath, IlpIntervalSolution
from optDfi.optimizations.ilp_opt.ilp_opt import OptDirected
from optDfi.optimizations.optimization_misc import LLVM_abc_change, LLVM_TAG_REORDER, LLVM_TAG_CHANGE, apply_changes, \
    scan_llvm_bytecode_metadata

COMBINED_ANALYSIS = Dict[ LOAD_ADD_HEX, Tuple[ DFILoad, List[ A3ValueAnalysisContext ] ] ]
INTERVAL_ORDER = Dict[ LoadId, Dict[ Tag, int ] ]

START_REPRESENTATION = 2
SPECIAL_TAG = '1'


class EmptyProblemException( Exception ) :
    pass


class UnsolvedILPException( Exception ) :
    pass


def transform_combined_analysis_into_ilp_problem( combined_analysis: COMBINED_ANALYSIS ) -> IlpIntervalProblem :
    contexts = [ ]

    for load_add in combined_analysis :
        bin_data, a3_data = combined_analysis[ load_add ]
        tags: Set[ Tag ] = set( bin_data.get_tags() | pipe.map( str ) )
        load_contexts = LoadContexts( loadid=str( bin_data.get_id() ), tags=tags )

        for context_data in a3_data :
            if context_data.count == 0 :
                continue
            context_tags = set( context_data.tags | pipe.map( str ) ).intersection( tags )
            if len( context_tags ) == 1 :
                # Exact c
                load_contexts.exacts.append( ExactContext( tag=str( context_tags.pop() ),
                                                           count=context_data.count ) )

            elif len( tags ) > len( context_tags ) > 1 :
                # Partial c
                load_contexts.partial.append( PartialContext( tags=frozenset( context_tags ),
                                                              count=context_data.count ) )
            else :
                # Unknown c
                load_contexts.unknown.append( UnknownContext( count=context_data.count ) )

        contexts.append( load_contexts )

    cp = ContextPath( contexts )
    problem = IlpIntervalProblem( path_contexts=cp, constraints=[ ],
                                  eqclass_loads=EqClassMap(), eqclass_tags=EqClassMap() )
    return problem


def combine_value_analysis_with_binary_info( a3_value_analysis: A3_VALUE_ANALYSIS,
                                             binary_data: List[ DFILoad ] ) -> COMBINED_ANALYSIS :
    dfi_load_db = { dfiload.start_add : dfiload for dfiload in binary_data }

    combine_analysis = dict( list( a3_value_analysis.items() )
                             | pipe.map(
        lambda x : (x[ 0 ], (dfi_load_db.get( int( x[ 0 ], base=16 ), None ), x[ 1 ])) )
                             | pipe.where( lambda x : x[ 1 ][ 0 ] is not None ) )

    return combine_analysis


def reconstruct_tag_representation( ilp_constructor: OptDirected, problem: IlpIntervalProblem ) -> Dict[ Tag, int ] :
    rev_raw_representation: Dict[ int, Tag ] = { round( ilp_constructor.vertex_index_vars[ t ].varValue ) : t for t in
                                                 ilp_constructor.vertex_index_vars }
    summed_added_tag: int = START_REPRESENTATION
    eqclass = problem.eqclass_tags
    representation: Dict[ Tag, int ] = { }

    # First compute the representation for '1' then continue with the rest of the representation

    for tag in eqclass[ SPECIAL_TAG ]:
        if tag == SPECIAL_TAG :
            representation[ tag ] = int( SPECIAL_TAG )
        else:
            representation[ tag ] = summed_added_tag
            summed_added_tag += 1

    # Then compute the rest of the representation, bypassing the SPECIAL_TAG when encountered
    for i in range( len( eqclass.current_cls ) ) :
        tag_class = rev_raw_representation[ i ]

        if SPECIAL_TAG in eqclass[ tag_class ]:
            continue

        for tag in eqclass[ tag_class ] :
            representation[ tag ] = summed_added_tag
            summed_added_tag += 1

    return representation


def reconstruct_interval_order( ilp_constructor: OptDirected, problem: IlpIntervalProblem ) -> INTERVAL_ORDER :
    interval_order = { }

    for loadid, tag_interval_dict in ilp_constructor.load_index_vars.items() :
        tag_order_dict = { tag: round( var.varValue ) for tag, var in tag_interval_dict.items() }

        for eqloadid in problem.eqclass_loads[ loadid ] :
            interval_order[ eqloadid ] = tag_order_dict

    return interval_order


def improve_problem( problem: IlpIntervalProblem ) -> IlpIntervalProblem :
    new_problem = problem. \
        remove_unoptimizable_contexts(). \
        inner_contexts_fusion(). \
        tag_equivalent_class(). \
        load_equivalent_class()

    return new_problem


def dump_problem( problem: IlpIntervalProblem, dir_path: Path, optimization_number: int ) :
    with open( dir_path / f"pb_{optimization_number}", 'wb' ) as f :
        pickle.dump( problem, f, pickle.HIGHEST_PROTOCOL )


def improve_problem_with_stats( problem: IlpIntervalProblem, stat_file: Path,
                                problem_path: Path ) -> IlpIntervalProblem :
    # Base problem stats
    base_ilp = OptDirected( problem )
    dump_problem( problem, problem_path, 0 )

    # First optimization
    ruc_problem = problem.remove_unoptimizable_contexts()
    ruc_ilp = OptDirected( ruc_problem )
    dump_problem( ruc_problem, problem_path, 1 )

    # Second optimization
    icf_problem = ruc_problem.inner_contexts_fusion()
    icf_ilp = OptDirected( icf_problem )
    dump_problem( icf_problem, problem_path, 2 )

    # Third optimization
    tag_problem = icf_problem.tag_equivalent_class()
    tag_ilp = OptDirected( tag_problem )
    dump_problem( tag_problem, problem_path, 3 )

    # Fourth optimization
    load_problem = tag_problem.load_equivalent_class()
    load_ilp = OptDirected( load_problem )
    dump_problem( load_problem, problem_path, 4 )

    with open( stat_file, 'w' ) as f :
        f.write( f"BASE PROBLEM,{base_ilp.model.numVariables()},{base_ilp.model.numConstraints()}\n" )
        f.write( f"RUC PROBLEM,{ruc_ilp.model.numVariables()},{ruc_ilp.model.numConstraints()}\n" )
        f.write( f"ICF PROBLEM,{icf_ilp.model.numVariables()},{icf_ilp.model.numConstraints()}\n" )
        f.write( f"TAG PROBLEM,{tag_ilp.model.numVariables()},{tag_ilp.model.numConstraints()}\n" )
        f.write( f"LOAD PROBLEM,{load_ilp.model.numVariables()},{load_ilp.model.numConstraints()}\n" )

    return load_problem


def merge_with_previous_problem( new_wcet_path_problem: IlpIntervalProblem,
                                 previous_problem: IlpIntervalProblem ) -> IlpIntervalProblem :
    problem = IlpIntervalProblem(
        path_contexts=new_wcet_path_problem.path_contexts,
        constraints=previous_problem.constraints,
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    return problem


def construct_next_problem( problem: IlpIntervalProblem, ilp_constructor: OptDirected, slack: int ) -> IlpIntervalProblem :
    slack_factor = 1 + (slack / 100)

    semi_opt_problem = problem.remove_unoptimizable_contexts().inner_contexts_fusion()

    next_problem = IlpIntervalProblem(
        path_contexts=ContextPath(),
        constraints=[
            (semi_opt_problem.path_contexts, int(slack_factor * int( ilp_constructor.model.objective.value() ))),
            *semi_opt_problem.constraints
        ],
        eqclass_tags=EqClassMap(),
        eqclass_loads=EqClassMap()
    )

    return next_problem


def generate_llvm_changes_from_solution( solution: IlpIntervalSolution, binary_data: List[ DFILoad ], original_ll_file: Path ) \
        -> List[ LLVM_abc_change ] :
    # Generate a list of changes to apply to the LLVM bytecode
    llvm_changes: List[ LLVM_abc_change ] = [ ]
    db = scan_llvm_bytecode_metadata( original_ll_file )

    dfi_load_id_db = { str( dfiload.get_id() ) : dfiload for dfiload in binary_data }

    # Generate the changes for the interval order
    for loadid, order in solution.interval_order.items() :
        load_bin_data = dfi_load_id_db[ loadid ]

        tag_order = sorted([ (tag, tag_interval) for tag, tag_interval in order.items() ], key=lambda x: x[1])

        llvm_changes.append( LLVM_TAG_REORDER(
            load_id=load_bin_data.get_id(),
            new_tag_order=list( tag_order | pipe.map( lambda x: int( x[0] ) ) )
        ) )

    full_tags = set( db.dfi_reverse_store_tag_id.keys() )

    # Generate a representation for the entire tags in the program (not just in the problem)
    new_tag_representation = { int(tag): new_tag for tag, new_tag in solution.tag_representation.items() }
    n = max(max( solution.tag_representation.values(), default=START_REPRESENTATION ) + 1, START_REPRESENTATION)
    for other_tag in (full_tags | pipe.where( lambda t : str(t) not in solution.tag_representation )) :
        if other_tag == 0 :
            new_tag_representation[ other_tag ] = 0
        else :
            new_tag_representation[ other_tag ] = n
            n += 1

    # Generate the changes for all the tags of the program (avoid problems of conflicts between tags)
    for previous_tag, new_tag in new_tag_representation.items() :
        llvm_changes.append( LLVM_TAG_CHANGE(
            previous_tag=int( previous_tag ),
            new_tag=new_tag
        ) )

    return llvm_changes


def find_tag_check_optimization( xml_file: Path, binary_file: Path, original_ll_file: Path, previous_solution: Optional[ IlpIntervalSolution ],
                                 problem_log: Path, time_limit: int, slack: int, ilp_pb_stats: Path, lp_file: Path, solution_json: Path,
                                 solution_file: Path ) \
        -> Tuple[ List[LLVM_abc_change], IlpIntervalSolution] :
    # Recover the a3 analysis results, the binary data and combine it
    a3_value_analysis = extract_wcep_context_data_from_xml( xml_file )
    binary_data = extract_dfi_loads( binary_file )
    combined_analysis = combine_value_analysis_with_binary_info( a3_value_analysis, binary_data )

    # Generate the problem based on the combined data
    new_wcet_path_problem: IlpIntervalProblem = transform_combined_analysis_into_ilp_problem( combined_analysis )
    if previous_solution is None:
        problem: IlpIntervalProblem = new_wcet_path_problem
    else:
        problem: IlpIntervalProblem = merge_with_previous_problem( new_wcet_path_problem, previous_solution.problem )

    # Optimize the problem to reduce the size of the ILP
    # optimized_problem: IlpIntervalProblem = improve_problem_with_stats( problem, problem_path=problem_log,
    #                                                                     stat_file=ilp_log )
    optimized_problem: IlpIntervalProblem = improve_problem( problem )

    if len( optimized_problem.path_contexts ) == 0 :
        raise EmptyProblemException()

    # Construct and solve the ILP
    ilp_constructor = OptDirected( optimized_problem, time_limit=time_limit )
    if previous_solution is not None:
        ilp_constructor.warm_up_from_previous_result( previous_solution )

    ilp_constructor.solve( lp_file=lp_file,
                           solution_path=solution_json )

    # if not ilp_constructor.check_result():
    #     raise UnsolvedILPException()

    # Construct the next_problem
    next_problem: IlpIntervalProblem = construct_next_problem( problem, ilp_constructor, slack )

    # Reconstruct the tag representation and the order of the intervals
    tag_representation = reconstruct_tag_representation( ilp_constructor, optimized_problem )
    interval_order = reconstruct_interval_order( ilp_constructor, optimized_problem )

    solution = IlpIntervalSolution( problem=next_problem, tag_representation=tag_representation,
                                    interval_order=interval_order )

    with open( problem_log / "solution", "wb" ) as f:
        pickle.dump( solution, f )

    llvm_changes = generate_llvm_changes_from_solution( solution=solution, binary_data=binary_data, original_ll_file=original_ll_file )

    new_solution = solution.apply_tag_map()

    if "CPLEX_CMD" in pulp.listSolvers(True) and config.DEFAULT_AIT_PATH is not None:
        with open( solution_file, "wb" ) as f :
            pickle.dump( new_solution, f )
    else:
        with open( solution_file, "rb" ) as f :
            new_solution = pickle.load( f )

    return llvm_changes, new_solution


def optimize_wcep_ilp( xml_file: Path, binary_file: Path, original_ll_file: Path,
                       optimized_ll_file: Path, previous_solution: IlpIntervalSolution,
                       problem_log: Path, ilp_pb_stats: Path, time_limit: int, slack: int, lp_file: Path, solution_json: Path,
                       solution_file: Path ) -> IlpIntervalSolution :
    changes, new_solution = find_tag_check_optimization( xml_file, binary_file, original_ll_file, previous_solution, problem_log, time_limit,
                                                         slack, ilp_pb_stats=ilp_pb_stats, lp_file=lp_file, solution_json=solution_json,
                                                         solution_file=solution_file )

    apply_changes(
        original_ll_file=original_ll_file,
        optimized_ll_file=optimized_ll_file,
        changes=changes,
        log_changes=problem_log
    )

    return new_solution
