# coding=utf-8
from pathlib import Path
from typing import Optional

ARCH_FLAGS = [
    "-march=riscv32",
    "-mattr=+m"
]
DEBUG_FLAGS = [
    "-g",
]
OPT_FLAGS = [ ]
CLANG_FLAGS = [
    "-fno-builtin",
    "-ffreestanding",
    "-fno-discard-value-names",
    "--target=riscv32",
    "-O1"
]
LLC_FLAGS = [
    "-O1"
]
GCC_FLAGS = [
    "-mcmodel=medany",
    "-O0",
    "-nostartfiles",
    "-nolibc",
    "-Wno-trigraphs",  # Disable huge warnings on trigraphs that bloat the output
]

LINKER_SCRIPTS = {
    "qemu-wcet" : "linker-qemu-wcet.ld"
}

DEFAULT_PROJECT_DIR = Path( "." )

DEFAULT_LLVM_PATH = DEFAULT_PROJECT_DIR / "llvm-project"
DEFAULT_PHASAR_PATH = DEFAULT_PROJECT_DIR / "phasar/build/tools/dfi_binding/dfi_binding"
DEFAULT_GCC_RISCV32_PATH = Path( "/opt/riscv32i/bin/riscv32-unknown-elf-gcc" )
DEFAULT_RISCV32_OBJDUMP_PATH = Path( "/opt/riscv32i/bin/riscv32-unknown-elf-objdump" )
DEFAULT_AIT_PATH: Optional[Path] = None # Path( "/usr/local/a3_riscv/bin/alauncher" )

DRY_RUN_FOLDER_PATH = Path("")

DEFAULT_LINK_PATH = DEFAULT_PROJECT_DIR / "DFI/dfioptwcet/ext/bootloader/"
APX_TEMPLATE_PATH = DEFAULT_PROJECT_DIR / "DFI/dfioptwcet/ext/template.apx"

ONLY_ANALYSIS = False
