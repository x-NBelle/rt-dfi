#!/usr/bin/env Rscript
library( ggplot2 )

args = commandArgs(trailingOnly=TRUE)
filename <- args[1]
data <- read.csv( filename, header=TRUE )

cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

data$RUNTIME = rowSums(data[,4:ncol(data)])
data$ILP_RUNTIME = data$ILP_0_ANALYSIS + data$ILP_1_ANALYSIS + data$ILP_2_ANALYSIS + data$ILP_3_ANALYSIS
data$WCET_RUNTIME = data$SOTA_WCET + data$PRE_VALUE_WCET + data$VALUE_ANALYSIS_WCET + data$ILP_0_WCET + data$ILP_1_WCET + data$ILP_2_WCET + data$ILP_3_WCET
data$BUILD_RUNTIME = data$SOTA_BUILD + data$PRE_VALUE_BUILD + data$VALUE_ANALYSIS_BUILD + data$ILP_0_BUILD + data$ILP_1_BUILD + data$ILP_2_BUILD + data$ILP_3_BUILD

mean(data$RUNTIME)
mean(data$ILP_RUNTIME)
mean(data$WCET_RUNTIME)
mean(data$BUILD_RUNTIME)

mean( data$ILP_RUNTIME / data$RUNTIME * 100)
mean( data$WCET_RUNTIME / data$RUNTIME * 100)
mean( data$BUILD_RUNTIME / data$RUNTIME * 100)

print(mean( data$RUNTIME / (data$SOTA_BUILD + data$SOTA_WCET)))
