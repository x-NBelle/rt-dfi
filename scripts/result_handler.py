# coding=utf-8

import csv
from pathlib import Path
from dataclasses import dataclass
from typing import List

file = Path( "./2022-01-19T19:42:15.963152/180s-0/result.csv" )
newfile = Path( "./result.csv" )
latexfile = Path( "./result.tex" )


@dataclass( frozen=True )
class ExpData( object ) :
    name: str
    vanilla: int
    sota: int
    value_analysis_plusplus: int
    ilp_first_round: int
    ilp_best_round: int
    nb_round: int


data: List[ ExpData ] = [ ]

if __name__ == "__main__" :
    with open( file, newline='' ) as csvfile :
        csvreader = csv.reader( csvfile, delimiter=',' )
        for row in csvreader :
            if len( row ) > 0 :
                data.append( ExpData( name=row[ 0 ], vanilla=int( row[ 1 ] ), sota=int( row[ 2 ] ),
                                      value_analysis_plusplus=int( row[ 3 ] ),
                                      ilp_first_round=int( row[ 4 ] ), ilp_best_round=int( row[ 5 ] ),
                                      nb_round=int( row[ 6 ] ) ) )

    data.sort( key=lambda x: x.name )

    with open( newfile, 'w', newline='' ) as csvfile :
        csvwriter = csv.writer( csvfile, dialect='unix', delimiter=';', quoting=csv.QUOTE_NONE )
        header = [ "Bench", "Vanilla", "SotA", "VA++", "ILP R1", "ILP Best", "It" ]

        csvwriter.writerow( header )
        for exp in data :
            baseline_overhead = (exp.sota - exp.vanilla) / exp.vanilla
            vaplus_overhead = (exp.value_analysis_plusplus - exp.vanilla) / exp.vanilla
            vaplus_improvement = (exp.value_analysis_plusplus - exp.vanilla) / (exp.sota - exp.vanilla) - 1
            r1_overhead = (exp.ilp_first_round - exp.vanilla) / exp.vanilla
            r1_improvement = (exp.ilp_first_round - exp.vanilla) / (exp.sota - exp.vanilla) - 1
            rbest_overhead = (exp.ilp_best_round - exp.vanilla) / exp.vanilla
            rbest_improvement = (exp.ilp_best_round - exp.vanilla) / (exp.sota - exp.vanilla) - 1

            new_data = [ exp.name[ 6 : ], f"{exp.vanilla:,}", f"{baseline_overhead * 100:.2f}%",
                         f"{vaplus_improvement * 100:.2f}%",
                         f"{r1_improvement * 100:.2f}%",
                         f"{rbest_improvement * 100:.2f}%",
                         f"{exp.nb_round}" ]

            csvwriter.writerow( new_data )

    with open( latexfile, 'w' ) as latexhandler :
        latexhandler.write( "\\begin{tabular}{|l|" + "r|" * 6 + "}\n" )
        latexhandler.write( "\\hline\n" )
        latexhandler.write(
            ' & '.join( [ "Bench", "Vanilla", "SotA", "VA++", "ILP R1", "ILP Best", "It" ] ) + "\\\\ \n" )
        latexhandler.write( "\\hline\n" )

        for exp in data :
            baseline_overhead = (exp.sota - exp.vanilla) / exp.vanilla
            vaplus_overhead = (exp.value_analysis_plusplus - exp.vanilla) / exp.vanilla
            vaplus_improvement = (exp.value_analysis_plusplus - exp.vanilla) / (exp.sota - exp.vanilla) - 1
            r1_overhead = (exp.ilp_first_round - exp.vanilla) / exp.vanilla
            r1_improvement = (exp.ilp_first_round - exp.vanilla) / (exp.sota - exp.vanilla) - 1
            rbest_overhead = (exp.ilp_best_round - exp.vanilla) / exp.vanilla
            rbest_improvement = (exp.ilp_best_round - exp.vanilla) / (exp.sota - exp.vanilla) - 1

            new_data = [ exp.name[ 6 : ].replace( '_', '\_' ), f"${exp.vanilla:,}$",
                         f"$+{baseline_overhead * 100:.2f}\%$",
                         f"${vaplus_improvement * 100:.2f}\%$",
                         f"${r1_improvement * 100:.2f}\%$",
                         f"${rbest_improvement * 100:.2f}\%$",
                         f"${exp.nb_round}$" ]

            latexhandler.write( ' & '.join( new_data ) + "\\\\ \n" )
            latexhandler.write( "\\hline\n" )

        latexhandler.write( "\\end{tabular}\n" )
