#!/usr/bin/env Rscript
library( ggplot2 )

args = commandArgs(trailingOnly=TRUE)
filename <- args[1]
data <- read.csv( filename, header=FALSE )
partial_data = data

cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")

order_bench <- function( bench ) {
    kernel_taclebench_benchname = c(
        "binarysearch",
        "bsort",
        "complex_updates",
        "cosf",
        "countnegative",
        "cubic",
        "deg2rad",
        "fft",
        "filterbank",
        "fir2dim",
        "iir",
        "insertsort",
        "isqrt",
        "jfdctint",
        "lms",
        "ludcmp",
        "matrix1",
        "md5",
        "minver",
        "pm",
        "prime",
        "rad2deg",
        "sha",  # Read in sha_wordcopy_fwd_aligned 8 bytes too much
        "st"
    )
    
    sequential_taclebench_benchname = c(
        "adpcm_dec",
        "adpcm_enc",
        # "ammunition",  # recursion
        # "anagram",  # recursion
        "audiobeam",
        "cjpeg_transupp",
        "cjpeg_wrbmp",
        "dijkstra",
        "epic",
        "fmref",
        "g723_enc",
        "gsm_dec",
        "gsm_enc",
        "h264_dec",
        "huff_dec",
        # "huff_enc",  # recursion
        "mpeg2",
        "ndes",
        "petrinet",
        "statemate",
        "susan"
    )
    
    test_taclebench_benchname = c(
        "cover",
        "duff",
        "test3"
    )
    
    app_taclebench_benchname = c(
        "lift",
        "powerwindow"
    )
    
    all_benchname = c( kernel_taclebench_benchname,
                       sequential_taclebench_benchname,
                       test_taclebench_benchname,
                       app_taclebench_benchname )
    
    match( as.character(bench), all_benchname )
}

bench_type = function(bench) {
    kernel_taclebench_benchname = c(
        "binarysearch",
        "bsort",
        "complex_updates",
        "cosf",
        "countnegative",
        "cubic",
        "deg2rad",
        "fft",
        "filterbank",
        "fir2dim",
        "iir",
        "insertsort",
        "isqrt",
        "jfdctint",
        "lms",
        "ludcmp",
        "matrix1",
        "md5",
        "minver",
        "pm",
        "prime",
        "rad2deg",
        "sha",  # Read in sha_wordcopy_fwd_aligned 8 bytes too much
        "st"
    )
    
    sequential_taclebench_benchname = c(
        "adpcm_dec",
        "adpcm_enc",
        # "ammunition",  # recursion
        # "anagram",  # recursion
        "audiobeam",
        "cjpeg_transupp",
        "cjpeg_wrbmp",
        "dijkstra",
        "epic",
        "fmref",
        "g723_enc",
        "gsm_dec",
        "gsm_enc",
        "h264_dec",
        "huff_dec",
        # "huff_enc",  # recursion
        "mpeg2",
        "ndes",
        "petrinet",
        "statemate",
        "susan"
    )
    
    test_taclebench_benchname = c(
        "cover",
        "duff",
        "test3"
    )
    
    app_taclebench_benchname = c(
        "lift",
        "powerwindow"
    )
    str_bench <- as.character(bench)
    
    result <- c()
    
    for ( index in 1:length(str_bench) ) {
        result <- c(result, 
            if ( str_bench[index] %in% kernel_taclebench_benchname ) {
                "kernel"
            } else if( str_bench[index] %in% sequential_taclebench_benchname ) {
                "sequential"
            } else if( str_bench[index] %in% test_taclebench_benchname ) {
                "test"
            } else if( str_bench[index] %in% app_taclebench_benchname ) {
                "app"
            } else {
                "other"
            }
        )
    }
    
    result
}

pdf(file="improvement.pdf", width=10, height=6)

benchmark <- c()
improvements <- c()
optimizations <- rep( c("value analysis improvement", "RT-DFI"
                    ), nrow(partial_data))

rtdfi_improvement = c()
va_improvement = c()

for ( row in 1:nrow(partial_data) ) {
        benchmark <- c(benchmark, rep(substring(as.character(partial_data[row,1]), 7), 2))

        baseline = partial_data[row, 3] - partial_data[row, 2]

        vaplus_diff = partial_data[row, 4] - partial_data[row, 2]
        improvements <- c( improvements, (1 - (vaplus_diff / baseline)) * 100 )
        va_improvement <- c( va_improvement, (1 - (vaplus_diff / baseline)) * 100 )

        r1_diff = partial_data[row, 5] - partial_data[row, 2]
        improvements <- c( improvements, (1 - (r1_diff / vaplus_diff)) * 100 )
        rtdfi_improvement <- c( rtdfi_improvement, (1 - (r1_diff / vaplus_diff)) * 100 )
}

benchOrder = order_bench( benchmark )
benchType = bench_type( benchmark )

va_mean_improvement = mean( va_improvement )
rtdfi_mean_improvement = va_mean_improvement + mean( rtdfi_improvement )

plot_data <- data.frame(benchmark, optimizations, improvements, benchOrder, benchType)
ggplot( plot_data, aes(fill=optimizations, y=improvements, 
                              x=reorder(benchmark, +benchOrder) ) ) +
    geom_bar( position="stack", stat="identity" ) +
    theme( axis.text.x = element_text(angle = 90, vjust=0.5, hjust=1, size=15 ),
           legend.position= c( 0.87, 0.9 ), text=element_text( size=15 ),
           legend.text = element_text( size=12 ),
           legend.title = element_text( size=12 )) +
    labs( y="improvement (%)", x="benchmarks" ) +
    scale_fill_manual( values = c(cbPalette[2], cbPalette[4]) ) + # Change the colors of the bars
    facet_grid(facets=~benchType, scales="free_x", space="free" ) +
    #geom_abline(slope=0, intercept=va_mean_improvement,  col =cbPalette[4],lty=2) +
    geom_abline(slope=0, intercept=rtdfi_mean_improvement,  col =cbPalette[1],lty=2)


dev.off()

full_improvements = c()
for ( row in 1:nrow(partial_data) ) {
        baseline = partial_data[row, 3] - partial_data[row, 2]

        r1_diff = partial_data[row, 5] - partial_data[row, 2]
        full_improvements <- c( full_improvements, (1 - (r1_diff / baseline)) * 100 )
}

print( mean(full_improvements) )
quantile( full_improvements, c(seq(0, 1, by=0.1) ) )
sd( full_improvements )

types = c()
for ( row in 1:nrow(partial_data) ) {
        baseline = partial_data[row, 3] - partial_data[row, 2]

        vaplus_diff = partial_data[row, 4] - partial_data[row, 2]
        vaplus_improv = (1 - (vaplus_diff / baseline)) * 100

        r1_diff = partial_data[row, 5] - partial_data[row, 2]
        r1_improv = (1 - (r1_diff / vaplus_diff)) * 100

        if ( vaplus_improv > r1_improv ) {
            types <- c( types, "va+" )
        } else {
            types <- c( types, "ilp" )
        }
}

type_data = data.frame( partial_data[,1], types )
print( type_data )

print( "va+" )
print( sum( type_data$types == "va+" ) )
print( "ilp" )
print( sum( type_data$types == "ilp" ) )